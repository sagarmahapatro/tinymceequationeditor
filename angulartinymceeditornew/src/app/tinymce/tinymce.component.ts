import { Component, OnInit , Input} from '@angular/core';

import tinymce from 'tinymce/tinymce';
import * as $ from 'jquery'

@Component({
  selector: 'tinymce',
  templateUrl: './tinymce.component.html',
  styleUrls: ['./tinymce.component.css']
})
export class TinymceComponent implements OnInit {

  tinyMceSettings:any;
  initialValue:any;
   
  @Input()
  readonly:boolean = false;

  @Input()
  content:boolean = false;
  @Input()
  id:any = Math.random();
  @Input()
  hidetoolbar:boolean = false;
  constructor() { }

  ngOnInit() {
    this.initialValue = this.content;
    if(this.readonly){
      this.tinyMceSettings = {
        theme_url: 'assets/themes/silver/theme.js',
        skin_url: 'assets/tinymce/ui/oxide',
        readonly : 1,
        elements : this.id,
        height: 420,
        mode: "readonly",
        inline: false,
        menubar: false
      }
    }else if(this.hidetoolbar){
      this.tinyMceSettings = {
        theme_url: 'assets/themes/silver/theme.js',
        skin_url: 'assets/tinymce/ui/oxide',
        height: 420,
        mode : "exact",
      plugins: 'tiny_mce_wiris',
      elements : this.id,
        inline: false,
        toolbar: '',
        menubar: false
        
      } 
    } else {
      this.tinyMceSettings = {
        theme_url: 'assets/themes/silver/theme.js',
        skin_url: 'assets/tinymce/ui/oxide',
        height: 420,
        mode : "exact",
        elements : this.id,
      plugins: 'tiny_mce_wiris',
        inline: false,
        toolbar: 'undo redo | formatselect | bold italic backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help | tiny_mce_wiris_formulaEditor tiny_mce_wiris_formulaEditorChemistry',
        menubar: false
        
      } 
    }
    
  }

}
