import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ChoosetempletComponent } from './views/choosetemplet/choosetemplet.component';

const routes: Routes = [
  {path:"",redirectTo:"/choosetemplet", pathMatch:"full"},
  {path:"choosetemplet",component:ChoosetempletComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
