import { Injectable } from '@angular/core';
import {TempletDetail} from '../model/TempletDetail';
import {TempletID} from '../model/TempletID';
import {FirstTemplet} from '../model/templets/FirstTemplet';
import {SecondTemplet} from '../model/templets/SecondTemplet';

import {ITempletDetail} from '../model/ITempletDetail';

@Injectable({
    providedIn: 'root'
})
export class AppService {
    private templets:Map<string,ITempletDetail> = new Map();
    private selectedTempletVal:ITempletDetail

    constructor(){
      const templet1:TempletDetail = new FirstTemplet();
      this.templets.set(templet1.templetID,templet1);

      const templet2:TempletDetail = new SecondTemplet();
      this.templets.set(templet2.templetID,templet2);
    }

    set selectedTemplet(templet:ITempletDetail){
      this.selectedTempletVal = templet;
      console.log("setSelectedTemplet " +this.selectedTempletVal.name);
    }
    get selectedTemplet():ITempletDetail{
      return  this.selectedTempletVal;
    }

    public getAllTemplets():ITempletDetail[]{
      let values:IterableIterator<ITempletDetail> = this.templets.values();
      let templetsAr:ITempletDetail[] =  Array.from(values);
      return templetsAr;
    }

    public getTempletFromID(id:string):ITempletDetail{
          return this.templets.get(id);
    }

}
