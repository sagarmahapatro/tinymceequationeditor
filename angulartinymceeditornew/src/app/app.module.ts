import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {MatCardModule} from '@angular/material/card';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {TinyMCEMaterialModule} from './material.module';
import { ChoosetempletComponent } from './views/choosetemplet/choosetemplet.component';
import { EditTempletModule } from './views/edittemplet/edittemplet.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule }   from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
     ChoosetempletComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    TinyMCEMaterialModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    EditTempletModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
