import {MatButtonModule, MatCheckboxModule,MatToolbarModule,MatSelectModule} from '@angular/material';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatDialogModule} from '@angular/material/dialog';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import {MatRadioModule} from '@angular/material/radio';
import {MatCardModule} from '@angular/material/card';

@NgModule({
  imports: [MatCardModule,MatRadioModule, FlexLayoutModule,MatButtonModule, MatCheckboxModule,MatToolbarModule,MatSelectModule,MatFormFieldModule,MatDialogModule],
  exports: [MatCardModule,MatRadioModule,FlexLayoutModule,MatButtonModule, MatCheckboxModule, MatToolbarModule,MatSelectModule,MatFormFieldModule,MatDialogModule],
})
export class TinyMCEMaterialModule { }