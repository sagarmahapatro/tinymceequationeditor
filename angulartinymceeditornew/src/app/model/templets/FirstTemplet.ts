import {TempletDetail} from '../TempletDetail';
import {TempletID} from '../TempletID';
export class FirstTemplet extends TempletDetail {
   constructor(){
       super();
       this.templetID = TempletID.firsttemplet
       this.name = "Quiz Item";
       this.templet = `<p contenteditable="false" ><strong>Question Text:</strong></p>
       <p  id="first_templet_question" class="first_templet_main_question edit_mode" contenteditable="true"></p>
       <p contenteditable="false"><strong>Correct Answer:</strong></p>
       <p id="first_templet_main_answare" class="first_templet_question edit_mode" contenteditable="true"></p>
       <p contenteditable="false"><strong>Distractor 1:</strong></p>
       <p id="first_templet_distractor_1"  class="first_templet_distractor edit_mode" contenteditable="true"></p>
       <p contenteditable="false"><strong>Distractor 2:</strong></p>
       <p  id="first_templet_distractor_2" class="first_templet_distractor edit_mode" contenteditable="true"></p>`;
   }
}