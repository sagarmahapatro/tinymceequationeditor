import {TempletDetail} from '../TempletDetail';
import {TempletID} from '../TempletID';
import * as $ from 'jquery'
export class SecondTemplet extends TempletDetail {
   question:String  ;
   constructor(){
       super();
       this.templetID = TempletID.secondtemplet
       this.name = " Individual Item";
       this.templet = `<p contenteditable="false" ><strong>Question Text:</strong></p>
       <p  id="second_templet_question" class="second_templet_main_question edit_mode" contenteditable="true"></p>
       <p contenteditable="false"><strong>Correct Answer:</strong></p>
       <p id="second_templet_main_answare" class="second_templet_main_answare edit_mode" contenteditable="true"></p>`;
    

   }
}