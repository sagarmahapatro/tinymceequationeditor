import {ITempletDetail} from './ITempletDetail'
export abstract class TempletDetail implements ITempletDetail {
   

    templetVal:string;
    contentVal:string;
    private templetIDArg:string;
    private nameArg:string

    constructor(){
    // console.log(" templetIDArg "+templetIDArg);
    }

    get templet():string {
        return this.templetVal;
    }

    set templet(arg:string) {
         this.templetVal = arg;
    }

    get content():string {
        return this.contentVal;
    }

    set content(arg:string) {
         this.contentVal = arg;
    }

    get templetID():string {
        return this.templetIDArg;
    }

    set templetID(arg:string) {
         this.templetIDArg = arg;
    }

    get name():string {
        return this.nameArg;
    }

    set name(arg:string) {
         this.nameArg = arg;
    }

    saveEditedContent(): void {
       // throw new Error("Method not implemented.");
    }
    getPreviewTemplet(): string {
       // throw new Error("Method not implemented.");
       return "";
    }

}