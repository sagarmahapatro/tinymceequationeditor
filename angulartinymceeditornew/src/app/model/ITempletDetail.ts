export interface  ITempletDetail{
    templet:string;
    content:string;
    templetID:string;
    name:string;

    saveEditedContent():void;
    getPreviewTemplet():string;
}