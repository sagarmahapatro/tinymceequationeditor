import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChoosetempletComponent } from './choosetemplet.component';

describe('ChoosetempletComponent', () => {
  let component: ChoosetempletComponent;
  let fixture: ComponentFixture<ChoosetempletComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChoosetempletComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChoosetempletComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
