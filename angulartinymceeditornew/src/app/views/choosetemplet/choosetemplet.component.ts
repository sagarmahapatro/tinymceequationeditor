import { Component, OnInit } from '@angular/core';
import {TempletDetail} from '../../model/TempletDetail';
import {AppService} from '../../services/AppService'
import { Router } from '@angular/router';
import {ITempletDetail} from '../../model/ITempletDetail'

@Component({
  selector: 'app-choosetemplet',
  templateUrl: './choosetemplet.component.html',
  styleUrls: ['./choosetemplet.component.css']
})
export class ChoosetempletComponent implements OnInit {
  public templets:ITempletDetail[] ;

  selectedTemplet:ITempletDetail;
  constructor(private appService:AppService, private router:Router) { 
    this.templets = appService.getAllTemplets();
  }

  ngOnInit() {
  }

  selecteTemplet(event){
    let id = event.value;
    this.selectedTemplet = this.appService.getTempletFromID(id);;
    console.log( id+" selectedTemplet "+this.selectedTemplet);
    this.navigateToTempletEditor()
  }

  navigateToTempletEditor(){
    this.appService.selectedTemplet =  this.selectedTemplet;
    this.router.navigateByUrl('/edittemplet/'+this.selectedTemplet.templetID);
  }

  

}
