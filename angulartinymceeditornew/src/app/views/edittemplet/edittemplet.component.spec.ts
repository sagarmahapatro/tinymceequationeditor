import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EdittempletComponent } from './edittemplet.component';

describe('EdittempletComponent', () => {
  let component: EdittempletComponent;
  let fixture: ComponentFixture<EdittempletComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EdittempletComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EdittempletComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
