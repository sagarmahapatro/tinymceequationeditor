import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EdittempletComponent } from './edittemplet.component';
import { SecondTemplet } from './templets/secondtemplet/SecondTemplet';
import { FirstTemplet } from './templets/firsttemplet/FirstTemplet'
import {TempletID} from '../../model/TempletID'

const edittempletRoutes: Routes = [
   { path: 'edittemplet',
    component: EdittempletComponent,
    children: [
         {
            path: TempletID.secondtemplet,
            component: SecondTemplet
          },
          {
            path: TempletID.firsttemplet,
            component: FirstTemplet
          }
    ]}
];

@NgModule({
    imports: [RouterModule.forChild(edittempletRoutes)],
    exports: [RouterModule]
  })
  export class EditTempletRouterModule { }