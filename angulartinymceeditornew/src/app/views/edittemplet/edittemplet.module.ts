import { NgModule }       from '@angular/core';
import{ EdittempletComponent} from './edittemplet.component'
import {FirstTemplet} from './templets/firsttemplet/FirstTemplet'
import {  SecondTemplet } from './templets/secondtemplet/SecondTemplet';
import { FirstTempletPreviewer } from './previewers/firsttemplet/FirstTempletPreviewer';
import { SecondTempletPreviewer } from './previewers/secondtemplet/SecondTempletPreviewer';
import {EditTempletRouterModule} from './edittemplet.routing.module'
import {TinyMCEMaterialModule} from '../../material.module';
import { FormsModule } from '@angular/forms';
import { TinymceComponent } from '../../tinymce/tinymce.component';
import { EditorModule } from '../../../../tinymce-angular-component/src/editor/editor.module';
@NgModule({
    declarations:[
        EdittempletComponent,
        FirstTemplet,
        SecondTemplet,
        FirstTempletPreviewer,
        SecondTempletPreviewer,
        TinymceComponent
    ],
    imports:[EditorModule,EditTempletRouterModule,TinyMCEMaterialModule,FormsModule],
    exports:[EdittempletComponent],
    entryComponents: [FirstTempletPreviewer, SecondTempletPreviewer]
}
)
export class EditTempletModule{
    constructor(){

    }

}