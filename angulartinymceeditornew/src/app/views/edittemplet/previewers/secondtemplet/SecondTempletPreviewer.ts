import {Component, OnInit,Inject} from '@angular/core'
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {DialogData} from '../../../../model/DialogData'
import {AppService} from '../../../../services/AppService';
@Component({
    selector: 'second-templet--previewer',
    templateUrl: './SecondTempletPreviewer.html',
    styleUrls: ['./SecondTempletPreviewer.css']
  })
  export class SecondTempletPreviewer implements OnInit{
    previewHtml: any = '';
     name :String;
  
    constructor(
      public dialogRef: MatDialogRef<SecondTempletPreviewer>,
      @Inject(MAT_DIALOG_DATA) public data: DialogData, private appService:AppService) {}
  
      onNoClick(): void {
        this.dialogRef.close();
      }
      ngOnInit(): void {
        this.appService.selectedTemplet.getPreviewTemplet();
        this.name = this.appService.selectedTemplet.name;
        this.previewHtml = window["tinyMCE"].activeEditor.getContent({format:'html'});
        this.previewHtml=this.previewHtml.replace(/class="*.*" /g, "");   
      }
      ngAfterViewInit(): void {
        window["tinyMCE"].activeEditor.setContent(this.previewHtml, { format: 'html' });
        window["tinyMCE"].activeEditor.setMode('readonly')
      }
  
  }