import { Component, OnInit } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {FirstTempletPreviewer} from '../edittemplet/previewers/firsttemplet/FirstTempletPreviewer';
import {SecondTempletPreviewer} from '../edittemplet/previewers/secondtemplet/SecondTempletPreviewer';
import { Router } from '@angular/router';
import {AppService} from  '../../services/AppService';
import {TempletID} from  '../../model/TempletID';

@Component({
  selector: 'app-edittemplet',
  templateUrl: './edittemplet.component.html',
  styleUrls: ['./edittemplet.component.css']
})
export class EdittempletComponent implements OnInit {
  selectedTempletName:string;

  constructor(public dialog: MatDialog, private router:Router, private appService:AppService) { 
    this.selectedTempletName = appService.selectedTemplet.name;
  }

  ngOnInit() {
  }
  lunchPreview(): void {
    this.appService.selectedTemplet.saveEditedContent();
    
    let dialogRef:any;
    if(this.appService.selectedTemplet.templetID == TempletID.firsttemplet){
      dialogRef = this.dialog.open(FirstTempletPreviewer, {
        width: '1600px',
        height: '600px',
        data: {name: "", animal: ""}
      });
    } else if(this.appService.selectedTemplet.templetID == TempletID.secondtemplet){
      dialogRef = this.dialog.open(SecondTempletPreviewer, {
        width: '1600px',
        height: '600px',
        data: {name: "", animal: ""}
      });
    }
    

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  navigateToTempletSelection():void {
   this.router.navigateByUrl("/choosetemplet");
  }
  
  
}
