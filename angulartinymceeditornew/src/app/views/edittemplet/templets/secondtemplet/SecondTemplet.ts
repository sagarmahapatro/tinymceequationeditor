import {Component, OnInit} from '@angular/core'
import {AppService} from  '../../../../services/AppService';
import {TempletID} from  '../../../../model/TempletID';
import {ITempletDetail} from  '../../../../model/ITempletDetail';

@Component({
    selector: 'second-templet',
    templateUrl: './SecondTemplet.html',
    styleUrls: ['./SecondTemplet.css']
})
export  class SecondTemplet implements OnInit{
    content:string;
    name:string;
    templetDetail:ITempletDetail;
    constructor(private appService:AppService){
        this.templetDetail = appService.getTempletFromID(TempletID.secondtemplet);
        this.name = this.templetDetail.name;
        this.content = this.templetDetail.templet;

    }
    ngOnInit(): void {
       
    }
}