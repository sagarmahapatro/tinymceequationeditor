import {Component, OnInit} from '@angular/core'
import {AppService} from  '../../../../services/AppService';
import {TempletID} from  '../../../../model/TempletID';
import {ITempletDetail} from  '../../../../model/ITempletDetail';

@Component({
    selector: 'first-tmplet',
    templateUrl: './FirstTemplet.html',
    styleUrls: ['./FirstTemplet.css']
})
export  class FirstTemplet implements OnInit{

    content:string;
    name:string;
    templetDetail:ITempletDetail

    constructor(private appService:AppService){
        this.templetDetail = appService.getTempletFromID(TempletID.firsttemplet);
        this.name = this.templetDetail.name;
        this.content = this.templetDetail.templet;
    }
    ngOnInit(): void {
       // throw new Error("Method not implemented.");
    }
}