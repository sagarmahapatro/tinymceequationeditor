package com.ness.equationeditor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class TinymceEquationEditorApplication {

	public static void main(String[] args) {
		SpringApplication.run(TinymceEquationEditorApplication.class, args);
	}

}
