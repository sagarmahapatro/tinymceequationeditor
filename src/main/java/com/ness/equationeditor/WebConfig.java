package com.ness.equationeditor;

import javax.servlet.http.HttpServlet;

import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.boot.web.servlet.server.ConfigurableServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.HttpServletBean;
import com.wiris.plugin.dispatchers.MainServlet;

@Configuration
public class WebConfig {
	
	   @Bean	
	   public ServletRegistrationBean<HttpServlet> countryServlet() {
		   ServletRegistrationBean<HttpServlet> servRegBean = new ServletRegistrationBean();
		   servRegBean.setServlet(new MainServlet());
		   servRegBean.addUrlMappings("/app/*");
		   servRegBean.setLoadOnStartup(1);
		   return servRegBean;
	   }
	   
	   @Bean
	   public WebServerFactoryCustomizer<ConfigurableServletWebServerFactory>
	     webServerFactoryCustomizer() {
	       return factory -> factory.setContextPath("/pluginwiris_engine");
	   }

}
