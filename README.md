# To run this application please follow below mentioned steps

## Build angular application

1. open command line from root of the project and run cd angulartinymceeditor 

1. run  npm install from command line

1. run ng build from command line

1. run cd.. from command line to go back to the root 

## Build spribngboot application

1. run mvn clean install   from command line 

1. run run.bat from command line 

## Start the Application

1. after successful deployment got to the browser and navigate to  http://localhost:8080/pluginwiris_engine/index.html  

https://github.com/tinymce/tinymce-angular
https://www.tiny.cloud/docs/demo/format-html5/#
https://www.tiny.cloud/docs/demo/pageembed/
https://www.tiny.cloud/docs-3x/reference/for-dummies/
https://github.com/ryu263/tinymce-visualblocks
https://www.tiny.cloud/docs/plugins/template/
https://www.dyclassroom.com/tinymce/how-to-set-data-to-tinymce-text-editor
